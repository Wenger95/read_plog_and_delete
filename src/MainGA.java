import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MainGA {
		
	 public static void main(String[] args) {

		String path1="/Users/Wenger95/Documents/kclaiproject/FightingICE/log/point/HPMode_Wenger_Tomatensimulator_";
		String path2=".PLOG";
		int gaScore[][]=new int [30][4];
		int temp[]=new int[4];

		for(int i=0;i<10;i++){
		try (BufferedReader br = new BufferedReader(new FileReader(path1+i+path2))) {
			String line;
			int j = i*3;
			//int j=0;
			while ((line = br.readLine()) != null) {
				String[] parts=line.split(",");
				temp[0]=Integer.parseInt( parts[0]);
				temp[1]=Integer.parseInt( parts[1]);
				temp[2]=Integer.parseInt( parts[2]);
				temp[3]=Integer.parseInt( parts[3]);
								
				gaScore[j]=temp.clone();
				j++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
			
		for(int i=0;i<30;i++){
			System.out.println(i+" Score " +1000*gaScore[i][1]/(gaScore[i][1]+gaScore[i][2]));
		}
		
		for (int i=0;i<10;i++){		
			File f = null;
		      boolean bool = false;
		      
		      try {
		      
		         // create new file
		         f = new File(path1+i+path2);
		         
		         // tries to delete a non-existing file
		         bool = f.delete();
		         
		         // creates file in the system
		         f.createNewFile();
		                 
		         // tries to delete the newly created file
		         bool = f.delete();
		         
		            
		      } catch(Exception e) {
		         // if any error occurs
		         e.printStackTrace();
		      }
		
		}
	 }
}
